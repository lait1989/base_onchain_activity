import getopt, sys
import random
import time
from depositEthToBase import depositEthToBase
from swapOnBase import swapETHOnBase, swapTokenToEth

# Arguments to script
argumentList = sys.argv[1:]

# Options
options = "h:i:r:s:p:"

# Long options
long_options = ["Help", "Initial_amount", "Randomize", "Sleep_range", "Percent_ETH"]

initial_amount = None
randomize = False
sleep_range = None
percent_ETH = '10,30'

try:
    # Parsing argument
    arguments, values = getopt.getopt(argumentList, options, long_options)

    # checking each argument
    for currentArgument, currentValue in arguments:
        if currentArgument in ("-h", "--Help"):
            print("You can use two arguments for script:"
                  "'-i' | '--Initial_amount' for amount to deposit. Required"
                  "'-r' | '--Randomize' for randomize wallets order. Default is False"
                  "'-s' | '--Sleep_range' for randomize time between wallets. Example: 1,5 (s)"
                  "'-p' | '--Percent_ETH' for randomize percent of ETH to swap on tokens. Example 10,30.")

        elif currentArgument in ("-i", "--Initial_amount"):
            initial_amount = currentValue
            print(f"Initial amount: {initial_amount}")

        elif currentArgument in ("-r", "--Randomize"):
            randomize = currentValue
            print(f"Randomize: {randomize}")

        elif currentArgument in ("-s", "--Sleep_range"):
            sleep_range = currentValue
            print(f"Sleep range: {sleep_range}")

        elif currentArgument in ("-p", "--Percent_ETH"):
            percent_ETH = currentValue
            print(f"Percent ETH: {percent_ETH}")

except getopt.error as err:
    # output error, and return with an error code
    print(str(err))

def getSleepInterval(sleep_range):
    interval = 1
    if (sleep_range):
        range = sleep_range.split(',')
        interval = random.randint(int(range[0]), int(range[1]))
    return interval

def getPercentEthToSwap(percent_ETH):
    range = percent_ETH.split(',')
    value = random.randint(int(range[0]), int(range[1]))
    return value

def readWallets():
    f = open('wallet.txt', 'r')
    wallets = f.read().splitlines()
    f.close()
    return wallets

if (initial_amount == None):
    print("You don't set initial amount for deposit")
else:
    try:
        wallets = readWallets()
        if (randomize):
            random.shuffle(wallets)

        for idx, key in enumerate(wallets):
            token_out = None
            index = idx + 1
            depositEthToBase(index, key, initial_amount)
            time.sleep(getSleepInterval(sleep_range))

            token_out = swapETHOnBase(index, key, getPercentEthToSwap(percent_ETH))
            time.sleep(getSleepInterval(sleep_range))

            if (token_out != None):
                swapTokenToEth(index, key, token_out)
                time.sleep(getSleepInterval(sleep_range))
            print('-------------------')


    except Exception as err:
        print(str(err))







