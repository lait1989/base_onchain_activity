import getopt, sys
import random

import hexbytes
from web3 import Web3
from eth_account import Account
from hexbytes import HexBytes
from eth_abi.packed import encode_packed
import json

base_rpc_url = "https://developer-access-mainnet.base.org"
sushi_swap_contract_address = "0x83eC81Ae54dD8dca17C3Dd4703141599090751D1"
sushi_swap_contract_abi = json.loads('''[{"inputs":[{"internalType":"address","name":"_bentoBox","type":"address"},{"internalType":"address[]","name":"priviledgedUserList","type":"address[]"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"address","name":"to","type":"address"},{"indexed":true,"internalType":"address","name":"tokenIn","type":"address"},{"indexed":true,"internalType":"address","name":"tokenOut","type":"address"},{"indexed":false,"internalType":"uint256","name":"amountIn","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amountOut","type":"uint256"}],"name":"Route","type":"event"},{"inputs":[],"name":"bentoBox","outputs":[{"internalType":"contract IBentoBoxMinimal","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"pause","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"tokenIn","type":"address"},{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"address","name":"tokenOut","type":"address"},{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"bytes","name":"route","type":"bytes"}],"name":"processRoute","outputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"}],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"resume","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"user","type":"address"},{"internalType":"bool","name":"priviledge","type":"bool"}],"name":"setPriviledge","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address payable","name":"transferValueTo","type":"address"},{"internalType":"uint256","name":"amountValueTransfer","type":"uint256"},{"internalType":"address","name":"tokenIn","type":"address"},{"internalType":"uint256","name":"amountIn","type":"uint256"},{"internalType":"address","name":"tokenOut","type":"address"},{"internalType":"uint256","name":"amountOutMin","type":"uint256"},{"internalType":"address","name":"to","type":"address"},{"internalType":"bytes","name":"route","type":"bytes"}],"name":"transferValueAndprocessRoute","outputs":[{"internalType":"uint256","name":"amountOut","type":"uint256"}],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"int256","name":"amount0Delta","type":"int256"},{"internalType":"int256","name":"amount1Delta","type":"int256"},{"internalType":"bytes","name":"data","type":"bytes"}],"name":"uniswapV3SwapCallback","outputs":[],"stateMutability":"nonpayable","type":"function"},{"stateMutability":"payable","type":"receive"}]''')

eth_address = '0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE'
weth_address = '0x4200000000000000000000000000000000000006'
tokens_out_addresses = ['0xd9aAEc86B65D86f6A7B5B1b0c42FFA531710b6CA', '0xEB466342C4d449BC9f53A865D5Cb90586f405215']
ERC20_abi = json.loads('''[{"inputs":[{"internalType":"address","name":"_bridge","type":"address"},{"internalType":"address","name":"_remoteToken","type":"address"},{"internalType":"uint8","name":"_decimals","type":"uint8"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"account","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Burn","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint8","name":"version","type":"uint8"}],"name":"Initialized","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"account","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Mint","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[],"name":"BRIDGE","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"REMOTE_TOKEN","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"bridge","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"burn","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"subtractedValue","type":"uint256"}],"name":"decreaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"addedValue","type":"uint256"}],"name":"increaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"_name","type":"string"},{"internalType":"string","name":"_symbol","type":"string"}],"name":"initialize","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"l1Token","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"l2Bridge","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"mint","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"remoteToken","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes4","name":"_interfaceId","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"pure","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"version","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"}]''')

def swapETHOnBase(idx, private_key, amount_procent = 30):
    # Connect to account by private key
    account = Account.from_key(private_key)
    # Get address
    address = account.address

    print(f"Wallet #{idx}, address: {address} is ready for swap ETH")

    # Connect to base_rpc_url
    web3 = Web3(Web3.HTTPProvider(base_rpc_url))
    print(f"Is connected: {web3.is_connected()} on BASE")

    # Get nonce
    nonce = web3.eth.get_transaction_count(address)

    # Get gas price
    gas_price = web3.eth.gas_price
    print(f"Gas price: {gas_price}")

    # Get eth balance on account
    eth_balance = web3.eth.get_balance(address)
    print(f"Eth balance on BASE is: {eth_balance / 10 ** 18}")

    if (eth_balance == 0):
        print("There is no amount of ETH on BASE chain")
        return None

    amount_to_swap = int(eth_balance * amount_procent / 100)
    token_out = random.choice(tokens_out_addresses)
    token_out_contract = web3.eth.contract(token_out, abi=ERC20_abi)
    token_out_name = token_out_contract.functions.name().call()
    token_out_decimals = token_out_contract.functions.decimals().call()

    if (token_out == '0xd9aAEc86B65D86f6A7B5B1b0c42FFA531710b6CA'):
        route = encode_packed(
            ['uint8','uint8','uint16','uint8','uint8','address','address','uint8','address','uint8','address','uint8','address'],
            [
                0x03,
                0x01,
                0xffff,
                0x02,
                0x01,
                web3.to_checksum_address('0xedc8f86740daafed0a38f1b73f9964aa5c80901c'),
                web3.to_checksum_address(weth_address),
                0x04,
                web3.to_checksum_address(weth_address),
                0x00,
                web3.to_checksum_address('0xedc8f86740daafed0a38f1b73f9964aa5c80901c'),
                0x01,
                web3.to_checksum_address(address)
            ])
    else:
        route = encode_packed(
            ['uint8','uint8','uint16','uint8','uint8','address','address','uint8','address','uint8','address','uint8','address'],
            [
                0x03,
                0x01,
                0xffff,
                0x02,
                0x01,
                web3.to_checksum_address('0x206a3356d7d4d2d2c9ebcd7cf489f1661a488ca1'),
                web3.to_checksum_address(weth_address),
                0x04,
                web3.to_checksum_address(weth_address),
                0x00,
                web3.to_checksum_address('0x206a3356d7d4d2d2c9ebcd7cf489f1661a488ca1'),
                0x01,
                web3.to_checksum_address(address)
            ])

    sushi_swap_contract = web3.eth.contract(sushi_swap_contract_address, abi=sushi_swap_contract_abi)
    estimate_gas = sushi_swap_contract.functions.processRoute(
        eth_address,
        amount_to_swap,
        token_out,
        0,
        address,
        route
    ).estimate_gas({"chainId": 8453, "from": address,"value": amount_to_swap})
    print(f"Estimate gas for swap ETH to {token_out_name}: {estimate_gas}")

    # Calculate gas amount
    gas_amount = gas_price * estimate_gas
    print(f"Gas amount: {gas_amount}")

    # Compare full amount with account balance
    if ((amount_to_swap + gas_amount) > eth_balance):
        print(f"You have no enougth amount for commision by swap ETH to {token_out_name}")
        return None

    # Build transaction
    process_route_transaction = (
        sushi_swap_contract
        .functions
        .processRoute(
            eth_address,
            amount_to_swap,
            token_out,
            0,
            address,
            route
        )
        .build_transaction({"chainId": 8453, "from": address, "value": amount_to_swap, "nonce": nonce}))

    # Sign transaction
    signed_tx = web3.eth.account.sign_transaction(process_route_transaction, private_key=private_key)
    # Send transaction
    send_tx = web3.eth.send_raw_transaction(signed_tx.rawTransaction)
    # Get transaction receipt
    tx_receipt = web3.eth.wait_for_transaction_receipt(send_tx)
    print(f"Transaction by swap ETH to {token_out_name}: {tx_receipt}")

    token_out_balance_raw = token_out_contract.functions.balanceOf(address).call()
    token_out_balance = token_out_balance_raw / 10**token_out_decimals
    print(f"Swap '{token_out}' ({token_out_name}) for {token_out_balance}")

    return token_out

def swapTokenToEth(idx, private_key, token):
    # Connect to account by private key
    account = Account.from_key(private_key)
    # Get address
    address = account.address

    print(f"Wallet #{idx}, address: {address} is ready for swap ETH")

    # Connect to base_rpc_url
    web3 = Web3(Web3.HTTPProvider(base_rpc_url))
    print(f"Is connected: {web3.is_connected()} on BASE")

    # Get nonce
    nonce = web3.eth.get_transaction_count(address)

    # Get gas price
    gas_price = web3.eth.gas_price
    print(f"Gas price: {gas_price}")

    # Get eth balance on account
    eth_balance = web3.eth.get_balance(address)
    print(f"Eth balance on BASE is: {eth_balance / 10 ** 18}")

    if (eth_balance == 0):
        print("There is no amount of ETH on BASE chain")
        return

    token_contract = web3.eth.contract(token, abi=ERC20_abi)
    token_name = token_contract.functions.name().call()
    token_balance = token_contract.functions.balanceOf(address).call()
    allowance = token_contract.functions.allowance(address, sushi_swap_contract_address).call()

    # Prepare route
    if (token == '0xd9aAEc86B65D86f6A7B5B1b0c42FFA531710b6CA'):
        route = encode_packed(
            ['uint8', 'address', 'uint8', 'uint16', 'uint8', 'address', 'uint8', 'address', 'uint8', 'address', 'uint8',
             'uint16', 'uint8', 'uint8', 'address'],
            [
                0x02,
                web3.to_checksum_address(token),
                0x01,
                0xffff,
                0x00,
                web3.to_checksum_address('0x41d160033c222e6f3722ec97379867324567d883'),
                0x00,
                web3.to_checksum_address(sushi_swap_contract_address),
                0x01,
                web3.to_checksum_address(weth_address),
                0x01,
                0xffff,
                0x02,
                0x00,
                web3.to_checksum_address(address)
            ])
    else:
        route = encode_packed(
            ['uint8', 'address', 'uint8', 'uint16', 'uint8', 'address', 'uint8', 'address', 'uint8', 'address', 'uint8',
             'uint16', 'uint8', 'uint8', 'address'],
            [
                0x02,
                web3.to_checksum_address(token),
                0x01,
                0xffff,
                0x00,
                web3.to_checksum_address('0x206a3356d7d4d2d2c9ebcd7cf489f1661a488ca1'),
                0x00,
                web3.to_checksum_address(sushi_swap_contract_address),
                0x01,
                web3.to_checksum_address(weth_address),
                0x01,
                0xffff,
                0x02,
                0x00,
                web3.to_checksum_address(address)
            ])

    if (allowance == 0):
        token_approve_estimate_gas = token_contract.functions.approve(
            sushi_swap_contract_address,
            token_balance
        ).estimate_gas({"chainId": 8453, "from": address})

        if (eth_balance < token_approve_estimate_gas):
            print(f"Can not approve {token_name}. Not enougth ETH for approve")
            return

        token_approve_transaction = (token_contract.functions.approve(
            sushi_swap_contract_address,
            token_balance
        ).build_transaction({"chainId": 8453, "from": address, "nonce": nonce}))

        # Sign transaction
        signed_approve_tx = web3.eth.account.sign_transaction(token_approve_transaction, private_key=private_key)
        # Send transaction
        send_approve_tx = web3.eth.send_raw_transaction(signed_approve_tx.rawTransaction)
        # Get transaction receipt
        tx_approve_receipt = web3.eth.wait_for_transaction_receipt(send_approve_tx)
        print(f"Transaction by approve {token_name}: {tx_approve_receipt}")

    sushi_swap_contract = web3.eth.contract(sushi_swap_contract_address, abi=sushi_swap_contract_abi)
    process_route_estimate_gas = sushi_swap_contract.functions.processRoute(
        token,
        token_balance,
        eth_address,
        0,
        address,
        route
    ).estimate_gas({"chainId": 8453, "from": address,"value": token_balance})

    if (eth_balance < process_route_estimate_gas):
        print(f"Can not swap {token_name} to ETH. Not enougth ETH for gas")
        return

    process_route_transaction = (sushi_swap_contract.functions.processRoute(
        token,
        token_balance,
        eth_address,
        0,
        address,
        route
    ).build_transaction({"chainId": 8453, "from": address,"value": token_balance, "nonce": nonce}))
    # Sign transaction
    signed_swap_tx = web3.eth.account.sign_transaction(process_route_transaction, private_key=private_key)
    # Send transaction
    send_swap_tx = web3.eth.send_raw_transaction(signed_swap_tx.rawTransaction)
    # Get transaction receipt
    tx_swap_receipt = web3.eth.wait_for_transaction_receipt(send_swap_tx)
    print(f"Transaction by swap {token_name} to ETH: {tx_swap_receipt}")





